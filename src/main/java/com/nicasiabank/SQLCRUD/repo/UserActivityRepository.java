package com.nicasiabank.SQLCRUD.repo;

import com.nicasiabank.SQLCRUD.model.Status;
import com.nicasiabank.SQLCRUD.model.UserActivitiy;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
@Repository
public interface UserActivityRepository extends PagingAndSortingRepository<UserActivitiy, Long> {

    @Query("select u from UserActivitiy u where u.users.id=?1 and u.status=?2")
    List<UserActivitiy> findByUserIdAndStatus(Long userId, Status status);
}
