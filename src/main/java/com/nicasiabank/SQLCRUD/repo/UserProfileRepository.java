package com.nicasiabank.SQLCRUD.repo;

import com.nicasiabank.SQLCRUD.dto.UserProfileResource;
import com.nicasiabank.SQLCRUD.model.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author rajim 2021-09-23.
 * @project IntelliJ IDEA
 */
@Repository
public interface UserProfileRepository extends PagingAndSortingRepository<Profile,
        Long> {
    @Query("select p from Profile  p where p.users.id=?1")
    Profile findByUserId(Long userId);
}
