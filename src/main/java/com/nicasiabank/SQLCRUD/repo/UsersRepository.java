package com.nicasiabank.SQLCRUD.repo;

import com.nicasiabank.SQLCRUD.model.Status;
import com.nicasiabank.SQLCRUD.model.Users;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author rajim 2021-09-20.
 * @project IntelliJ IDEA
 */
@Repository
public interface UsersRepository extends PagingAndSortingRepository<Users, Long> {

    Users findByUsernameAndPasswordAndStatus(String username, String password, Status status );


    @Query("select u from Users u where u.username=?1 and u.address=?2 and u.status = 1")
    Object findAllActive(String usernam, String address);

    Users findByUsername(String username);

    Users findByUsernameLike(String username);
}
