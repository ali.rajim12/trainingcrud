package com.nicasiabank.SQLCRUD.controller;

import com.nicasiabank.SQLCRUD.convert.UserConverter;
import com.nicasiabank.SQLCRUD.convert.UserProfileConverter;
import com.nicasiabank.SQLCRUD.dto.CreateUserProfileDto;
import com.nicasiabank.SQLCRUD.dto.GenericResponseDto;
import com.nicasiabank.SQLCRUD.dto.UserProfileResource;
import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.Profile;
import com.nicasiabank.SQLCRUD.service.UserProfileService;
import com.nicasiabank.SQLCRUD.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author rajim 2021-09-23.
 * @project IntelliJ IDEA
 */
@RestController
@RequestMapping(value = "/api/v1/profile")
public class UserProfileController {

    private final UserProfileService userProfileService;
    private final UserService userService;
    private final UserProfileConverter userProfileConverter;
    private final UserConverter userConverter;


    public UserProfileController(UserProfileService userProfileService,
                                 UserService userService,
                                 UserProfileConverter userProfileConverter,
                                 UserConverter userConverter) {
        this.userProfileService = userProfileService;
        this.userService = userService;
        this.userProfileConverter = userProfileConverter;
        this.userConverter = userConverter;
    }

    @PostMapping
    public GenericResponseDto createUserProfile(@RequestBody
                                                @Valid CreateUserProfileDto
                                                createUserProfileDto) {
        return userProfileService.createUserProfile(createUserProfileDto,
                userService.createNewUser(
                        userProfileService.convertToCreateUserDto(createUserProfileDto)));
    }

    @GetMapping("/{userId}")
    public UserProfileResource getUserProfileDetail(@PathVariable(value = "userId")
                                                    Long userId) {
        Profile userProfileDetail = userProfileService.getUserProfileDetail(userId);
        UserResource userResource = userConverter.convertUser(userProfileDetail.getUsers());
        return userProfileConverter.convertToUserProfile(userProfileDetail, userResource);

    }
}
