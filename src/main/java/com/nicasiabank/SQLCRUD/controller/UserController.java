package com.nicasiabank.SQLCRUD.controller;

import com.nicasiabank.SQLCRUD.convert.UserConverter;
import com.nicasiabank.SQLCRUD.dto.CreateUserDto;
import com.nicasiabank.SQLCRUD.dto.PageResult;
import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.Users;
import com.nicasiabank.SQLCRUD.service.UserService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

/**
 * @author rajim 2021-09-20.
 * @project IntelliJ IDEA
 */

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserController {

    private final UserService userService;

    private final UserConverter userConverter;

    public UserController(UserService userService,
                          UserConverter userConverter) {
        this.userService = userService;
        this.userConverter = userConverter;
    }

    @PostMapping
    public UserResource ceateNewUser(@RequestBody @Valid CreateUserDto createUserDto) {
        return userConverter.convertUser(userService.createNewUser(createUserDto));
    }

    @GetMapping
    public List<UserResource> getAllUsers() {
        return userService.getAllUsers();
    }

    /**
     * Import always data.domain pageable
     * @return
     */
    @GetMapping("/page")
    public PageResult<UserResource> getAllUsersInPage(Pageable pageable) {
        // In order to sort detail by any column name
        Pageable pageable1 = PageRequest.of(pageable.getPageNumber() ,
        pageable.getPageSize(), Sort.by(
                Sort.Order.asc("username")));
        return userService.getAllUsersPage(pageable1);
    }



    @GetMapping("/test")
    public Users getTest() {
        return userService.getTest();
    }
}
