package com.nicasiabank.SQLCRUD.controller;

import com.nicasiabank.SQLCRUD.convert.UserActivityConveter;
import com.nicasiabank.SQLCRUD.convert.UserConverter;
import com.nicasiabank.SQLCRUD.dto.ActivityDto;
import com.nicasiabank.SQLCRUD.dto.GenericResponseDto;
import com.nicasiabank.SQLCRUD.dto.UserActivityResource;
import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.UserActivitiy;
import com.nicasiabank.SQLCRUD.model.Users;
import com.nicasiabank.SQLCRUD.service.UserActivityService;
import com.nicasiabank.SQLCRUD.service.UserService;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/v1/userActivity")
public class UserActivityController {

    private final UserActivityService userActivityService;

    private final UserService userService;

    private final UserConverter userConverter;

    private final UserActivityConveter userActivityConveter;

    public UserActivityController(UserActivityService userActivityService,
                                  UserService userService,
                                  UserConverter userConverter, UserActivityConveter userActivityConveter) {
        this.userActivityService = userActivityService;
        this.userService = userService;
        this.userConverter = userConverter;
        this.userActivityConveter = userActivityConveter;
    }

    @PostMapping("/{userId}")
    public GenericResponseDto createUserActivity(@PathVariable(value = "userId") Long userId) {
        Users byUserId = userService.findByUserId(userId);
        return userActivityService.createUserActivity(byUserId);
    }

    @GetMapping("/{userId}")
    public UserActivityResource getAllUserActivities(@PathVariable(value = "userId") Long userId) {
        // This line are based on creating new variables
//        Users users = userService.findByUserId(userId);
//        UserResource userResource = userConverter.convertUser(userService.findByUserId(userId));
//        List<UserActivitiy> userActivitiyList = userActivityService.findAllByUserId(userId);
//
//        List<ActivityDto> activityDtoList = userActivityConveter.convertAllUserActivity(
//                userActivitiyList
//        );
        // HERE we have replaced all the variables
        return UserActivityResource.builder()
                .userResource(userConverter.convertUser(userService.findByUserId(userId)))
                .activityDtoList(userActivityConveter.convertAllUserActivity(
                        userActivityService.findAllByUserId(userId)
                ))
                .build();

    }


}
