package com.nicasiabank.SQLCRUD.convert;

import com.nicasiabank.SQLCRUD.dto.UserProfileResource;
import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.Profile;

/**
 * @author rajim 2021-09-24.
 * @project IntelliJ IDEA
 */
public interface UserProfileConverter {
    UserProfileResource convertToUserProfile(Profile userProfileDetail, UserResource userResource);
}
