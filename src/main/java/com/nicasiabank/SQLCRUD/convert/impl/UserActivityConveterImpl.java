package com.nicasiabank.SQLCRUD.convert.impl;

import com.nicasiabank.SQLCRUD.convert.UserActivityConveter;
import com.nicasiabank.SQLCRUD.dto.ActivityDto;
import com.nicasiabank.SQLCRUD.model.UserActivitiy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class UserActivityConveterImpl implements UserActivityConveter {

    Function<UserActivitiy, ActivityDto > userActivitiyActivityDtoFunction = userActivitiy ->
            ActivityDto.builder()
                    .loginDateTime(userActivitiy.getLoginDateTime())
                    .remarks(userActivitiy.getRemarks())
                    .status(userActivitiy.getStatus())
                    .build();

    @Override
    public List<ActivityDto> convertAllUserActivity(List<UserActivitiy> userActivitiyList) {
        if(userActivitiyList.size() == 0) {
            return new ArrayList<>();
        }
        return userActivitiyList.stream()
                .map(e-> userActivitiyActivityDtoFunction.apply(e))
                .collect(Collectors.toList());
    }
}
