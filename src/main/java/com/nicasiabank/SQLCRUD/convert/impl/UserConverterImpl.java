package com.nicasiabank.SQLCRUD.convert.impl;

import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.Users;
import com.nicasiabank.SQLCRUD.convert.UserConverter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-22.
 * @project IntelliJ IDEA
 */
@Service
public class UserConverterImpl implements UserConverter {

    Function<Users, UserResource> usersUserResourceFunction = users ->
            UserResource.builder()
                    .address(users.getAddress())
                    .email(users.getEmail())
                    .status(users.getStatus().name()) // to convert enum into string
                    .username(users.getUsername())
                    .userId(users.getId())
                    .build();

    @Override
    public List<UserResource> convertUserList(List<Users> usersList) {
        return usersList.stream()
                .map(e-> usersUserResourceFunction.apply(e))
                .collect(Collectors.toList());
    }

    @Override
    public UserResource convertUser(Users createdUsers) {
        return usersUserResourceFunction.apply(createdUsers);
    }
}
