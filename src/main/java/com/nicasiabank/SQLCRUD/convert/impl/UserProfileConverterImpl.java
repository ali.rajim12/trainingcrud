package com.nicasiabank.SQLCRUD.convert.impl;

import com.nicasiabank.SQLCRUD.convert.UserProfileConverter;
import com.nicasiabank.SQLCRUD.dto.UserProfileResource;
import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.Profile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author rajim 2021-09-24.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class UserProfileConverterImpl implements UserProfileConverter {

    @Override
    public UserProfileResource convertToUserProfile(Profile userProfileDetail,
                                                    UserResource userResource) {
        return UserProfileResource.builder()
                .age(userProfileDetail.getAge())
                .fullName(userProfileDetail.getFullName())
                .userResource(userResource)
                .build();
    }
}
