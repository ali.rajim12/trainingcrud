package com.nicasiabank.SQLCRUD.convert;

import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.Users;

import java.util.List;

/**
 * @author rajim 2021-09-22.
 * @project IntelliJ IDEA
 */
public interface UserConverter {
    List<UserResource> convertUserList(List<Users> usersList);

    UserResource convertUser(Users users);
}
