package com.nicasiabank.SQLCRUD.convert;

import com.nicasiabank.SQLCRUD.dto.ActivityDto;
import com.nicasiabank.SQLCRUD.model.UserActivitiy;

import java.util.List;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
public interface UserActivityConveter {
    List<ActivityDto> convertAllUserActivity(List<UserActivitiy> userActivitiyList);
}
