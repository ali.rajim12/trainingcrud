package com.nicasiabank.SQLCRUD.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author rajim 2021-09-23.
 * @project IntelliJ IDEA
 */
@Data
@Entity
public class UserActivitiy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModified;

    @Version
    private Long version = 1L;

    private Date loginDateTime;

    private String remarks;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users users;

    private Status status;
}
