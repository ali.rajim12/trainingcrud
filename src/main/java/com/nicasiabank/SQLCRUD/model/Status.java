package com.nicasiabank.SQLCRUD.model;

/**
 * @author rajim 2021-09-20.
 * @project IntelliJ IDEA
 */
public enum Status {
    INACTIVE, //0
    ACTIVE,  //1
    DELETED //3
}
