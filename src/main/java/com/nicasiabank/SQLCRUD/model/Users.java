package com.nicasiabank.SQLCRUD.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.ConstraintComposition;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author rajim 2021-09-20.
 * @project IntelliJ IDEA
 */
@Setter
@Getter
@Entity
/**
 *
 */
//@Table(uniqueConstraints =
//        { @UniqueConstraint(columnNames = { "username", "password" }) })
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModified;

    @Version
    private Long version = 1L;

    @Column(length = 10, unique = true)
    @NotNull
    private String username;

    private String password;

    private String email;

    private Status status;

    @Column(length = 100)
    private String address;

}
