package com.nicasiabank.SQLCRUD.service;

import com.nicasiabank.SQLCRUD.dto.GenericResponseDto;
import com.nicasiabank.SQLCRUD.model.UserActivitiy;
import com.nicasiabank.SQLCRUD.model.Users;

import java.util.List;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
public interface UserActivityService {
    GenericResponseDto createUserActivity(Users byUserId);

    List<UserActivitiy> findAllByUserId(Long userId);
}
