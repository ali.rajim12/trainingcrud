package com.nicasiabank.SQLCRUD.service;

import com.nicasiabank.SQLCRUD.dto.CreateUserDto;
import com.nicasiabank.SQLCRUD.dto.PageResult;
import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.model.Users;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author rajim 2021-09-20.
 * @project IntelliJ IDEA
 */
public interface UserService {
    Users createNewUser(CreateUserDto users);

    List<UserResource> getAllUsers();

    Users getTest();

    PageResult<UserResource> getAllUsersPage(Pageable pageable);

    Users findByUserId(Long userId);
}
