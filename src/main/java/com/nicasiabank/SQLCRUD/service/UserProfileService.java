package com.nicasiabank.SQLCRUD.service;

import com.nicasiabank.SQLCRUD.dto.CreateUserDto;
import com.nicasiabank.SQLCRUD.dto.CreateUserProfileDto;
import com.nicasiabank.SQLCRUD.dto.GenericResponseDto;
import com.nicasiabank.SQLCRUD.dto.UserProfileResource;
import com.nicasiabank.SQLCRUD.model.Profile;
import com.nicasiabank.SQLCRUD.model.Users;

/**
 * @author rajim 2021-09-23.
 * @project IntelliJ IDEA
 */
public interface UserProfileService {
    GenericResponseDto createUserProfile(CreateUserProfileDto createUserProfileDto, Users newUser);

    CreateUserDto convertToCreateUserDto(CreateUserProfileDto createUserProfileDto);

    Profile getUserProfileDetail(Long userId);
}
