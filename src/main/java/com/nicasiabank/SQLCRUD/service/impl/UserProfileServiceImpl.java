package com.nicasiabank.SQLCRUD.service.impl;

import com.nicasiabank.SQLCRUD.dto.CreateUserDto;
import com.nicasiabank.SQLCRUD.dto.CreateUserProfileDto;
import com.nicasiabank.SQLCRUD.dto.GenericResponseDto;
import com.nicasiabank.SQLCRUD.dto.UserProfileResource;
import com.nicasiabank.SQLCRUD.model.Profile;
import com.nicasiabank.SQLCRUD.model.Users;
import com.nicasiabank.SQLCRUD.repo.UserProfileRepository;
import com.nicasiabank.SQLCRUD.repo.UsersRepository;
import com.nicasiabank.SQLCRUD.service.UserProfileService;
import com.nicasiabank.SQLCRUD.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * @author rajim 2021-09-23.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class UserProfileServiceImpl implements UserProfileService {

    private final UserProfileRepository userProfileRepository;

    private final UsersRepository usersRepository;

//    private final UserService userService;

    public UserProfileServiceImpl(UserProfileRepository userProfileRepository,
                                  UsersRepository usersRepository) {
        this.userProfileRepository = userProfileRepository;
        this.usersRepository = usersRepository;
//        this.userService = userService;
    }

    @Override
    @Transactional
    public GenericResponseDto createUserProfile(CreateUserProfileDto createUserProfileDto,
                                                Users newUser) {
        log.info("Creating user profile with dto :: {}",
                createUserProfileDto.toString());
//        Users newUser = userService.createNewUser(convertToCreateUserDto(createUserProfileDto));
        // Before removing duplicate user creation code
//        Users users = usersRepository.findByUsername(createUserProfileDto.getUsername());
//        if(users != null) {
//            throw new ClientException("User already available.");
//        }
//        Users users1 = new Users();
//        users1.setStatus(Status.ACTIVE);
//        users1.setAddress(createUserProfileDto.getAddress());
//        users1.setCreatedDate(new Date());
//        users1.setEmail(createUserProfileDto.getEmail());
//        users1.setLastModified(new Date());
//        users1.setPassword(createUserProfileDto.getPassword());
//        users1.setUsername(createUserProfileDto.getUsername());
//        Users savedUser = usersRepository.save(users1);

        Profile profile = new Profile();
        profile.setAge(createUserProfileDto.getAge());
        profile.setFullName(createUserProfileDto.getFullName());
        profile.setCreatedDate(new Date());
        profile.setLastModified(new Date());
        profile.setUsers(newUser);
        userProfileRepository.save(profile);

        return GenericResponseDto.builder()
                .code(0)
                .message("User profile created.")
                .build();
    }

    @Override
    public CreateUserDto convertToCreateUserDto(CreateUserProfileDto createUserProfileDto) {
        CreateUserDto createUserDto = new CreateUserDto();
        createUserDto.setAddress(createUserProfileDto.getAddress());
        createUserDto.setEmail(createUserProfileDto.getEmail());
        createUserDto.setPassword(createUserProfileDto.getPassword());
        createUserDto.setUsername(createUserProfileDto.getUsername());
        return createUserDto;
    }

    @Override
    public Profile getUserProfileDetail(Long userId) {
        log.info("Finding user profile detail by userId:: {}", userId);
        return userProfileRepository.findByUserId(userId);
    }
}
