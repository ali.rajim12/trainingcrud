package com.nicasiabank.SQLCRUD.service.impl;

import com.nicasiabank.SQLCRUD.convert.UserConverter;
import com.nicasiabank.SQLCRUD.dto.CreateUserDto;
import com.nicasiabank.SQLCRUD.dto.PageResult;
import com.nicasiabank.SQLCRUD.dto.UserResource;
import com.nicasiabank.SQLCRUD.exception.ClientException;
import com.nicasiabank.SQLCRUD.model.Status;
import com.nicasiabank.SQLCRUD.model.Users;
import com.nicasiabank.SQLCRUD.repo.UsersRepository;
import com.nicasiabank.SQLCRUD.service.UserProfileService;
import com.nicasiabank.SQLCRUD.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.*;

/**
 * @author rajim 2021-09-20.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UsersRepository usersRepository;
    private final UserConverter userConverter;
    private final UserProfileService userProfileService;

    public UserServiceImpl(UsersRepository usersRepository,
                           UserConverter userConverter,
                           UserProfileService userProfileService) {
        this.usersRepository = usersRepository;
        this.userConverter = userConverter;
        this.userProfileService = userProfileService;
    }

    @Override
    public Users createNewUser(CreateUserDto createUserDto) {
        Users users = usersRepository.findByUsername(createUserDto.getUsername());
        if(users != null) {
            throw new ClientException("User already available.");
        }
        Users users1 = new Users();
        users1.setStatus(Status.ACTIVE);
        users1.setAddress(createUserDto.getAddress());
        users1.setCreatedDate(new Date());
        users1.setEmail(createUserDto.getEmail());
        users1.setLastModified(new Date());
        users1.setPassword(createUserDto.getPassword());
        users1.setUsername(createUserDto.getUsername());

        Users createdUsers = usersRepository.save(users1);
//        return userConverter.convertUser(createdUsers);
        return createdUsers;
    }

    @Override
    public List<UserResource> getAllUsers() {
        List<Users> usersList = (List<Users>) usersRepository.findAll();
        if(usersList.size() == 0) {
            return new ArrayList<>();
        }
        return userConverter.convertUserList(usersList);
    }

    @Override
    public Users getTest() {
//        return usersRepository.findByUsernameAndPasswordAndStatus(
//                "9849428178",
//                "test12"
//                ,Status.ACTIVE
//        );
        return usersRepository.findByUsernameLike("9849428178");
    }

    @Override
    public PageResult<UserResource> getAllUsersPage(Pageable pageable) {
        Page<Users> usersPage = usersRepository.findAll(pageable);
        if(usersPage.getContent().size() == 0) {
            return new PageResult<>();
        }
       // return null;
//        List<Users> content = usersPage.getContent();
        return new PageResult<>(
                userConverter.convertUserList(usersPage.getContent()),
                usersPage.getNumber(),
                usersPage.getTotalElements(),
                usersPage.getTotalPages());

    }

    @Override
    public Users findByUserId(Long userId) {
        return usersRepository.findById(userId)
                .orElseThrow(()->
                        new ClientException("Detail not found"));
    }

}
