package com.nicasiabank.SQLCRUD.service.impl;

import com.nicasiabank.SQLCRUD.dto.GenericResponseDto;
import com.nicasiabank.SQLCRUD.model.Status;
import com.nicasiabank.SQLCRUD.model.UserActivitiy;
import com.nicasiabank.SQLCRUD.model.Users;
import com.nicasiabank.SQLCRUD.repo.UserActivityRepository;
import com.nicasiabank.SQLCRUD.service.UserActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class UserActivityServiceImpl implements UserActivityService {
    private final UserActivityRepository userActivityRepository;

    public UserActivityServiceImpl(UserActivityRepository userActivityRepository) {
        this.userActivityRepository = userActivityRepository;
    }

    @Override
    public GenericResponseDto createUserActivity(Users users) {
        UserActivitiy userActivitiy = new UserActivitiy();
        userActivitiy.setCreatedDate(new Date());
        userActivitiy.setLastModified(new Date());
        userActivitiy.setLoginDateTime(new Date());
        userActivitiy.setRemarks("Logged in success.");
        userActivitiy.setStatus(Status.ACTIVE);
        userActivitiy.setUsers(users);
        userActivityRepository.save(userActivitiy);
        return GenericResponseDto.builder()
                .message("Activity saved successfully.")
                .code(200)
                .build();
    }

    @Override
    public List<UserActivitiy> findAllByUserId(Long userId) {
        return userActivityRepository.findByUserIdAndStatus(userId, Status.ACTIVE);
    }
}
