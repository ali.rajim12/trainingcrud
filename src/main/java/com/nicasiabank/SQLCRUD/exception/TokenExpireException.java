package com.nicasiabank.SQLCRUD.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author rajim 2021-09-17.
 * @project IntelliJ IDEA
 */
@Getter
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class TokenExpireException extends RuntimeException {

    public TokenExpireException(String message) {
        super(message);
    }
}
