package com.nicasiabank.SQLCRUD.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author rajim 2021-09-17.
 * @project IntelliJ IDEA
 */
@Getter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ClientException extends RuntimeException {

    public ClientException(String message) {
        super(message);
    }
}
