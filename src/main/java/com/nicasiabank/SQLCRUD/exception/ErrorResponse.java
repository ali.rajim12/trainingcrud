package com.nicasiabank.SQLCRUD.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author rajim 2021-09-17.
 * @project IntelliJ IDEA
 */
@Getter
@JsonPropertyOrder(value = {"code", "path", "message", "stack_trace"})
public class ErrorResponse implements Serializable {

    private final String message;

    @JsonProperty("stack_trace")
    private final String stackTrace;

    private final String path;

    private final int code;

    public ErrorResponse(int code,
                         String message,
                         String path) {
        this(code, path, message, null);
    }

    public ErrorResponse(int code,
                         String path,
                         String message,
                         String stackTrace) {
        this.code = code;
        this.path = path;
        this.message = message;
        this.stackTrace = stackTrace;
    }
}

