//package com.nicasiabank.SQLCRUD.exception;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.validation.ConstraintViolationException;
//
///**
// * @author rajim 2021-09-21.
// * @project IntelliJ IDEA
// */
//@RestController
//@Validated
//public class ValidateParametersController {
//    @ExceptionHandler(ConstraintViolationException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
//        return new ResponseEntity<>("not valid due to validation error: " + e.getMessage(),
//                HttpStatus.BAD_REQUEST);
//    }
//}
