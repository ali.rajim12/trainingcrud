package com.nicasiabank.SQLCRUD.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

/**
 * @author rajim 2021-09-17.
 * @project IntelliJ IDEA
 */
@Slf4j
@ControllerAdvice
public class ControllerExceptionAdvice {

    /**
     * Bad Request exception handler
     * @param rna client exception
     * @param request
     * @return ErrorResponse with message
     */


    @ExceptionHandler(ClientException.class)
    public ResponseEntity<ErrorResponse> clientException(final ClientException rna,
                                                         final HttpServletRequest request) {
        log.error("Exception:: {}", rna.getStackTrace());
        final HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(new ErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                rna.getMessage(),
                request.getRequestURI()),
                status);
    }


    @ExceptionHandler(TokenExpireException.class)
    public ResponseEntity<ErrorResponse> tokenExpiryException(final TokenExpireException rna,
                                                         final HttpServletRequest request) {
        log.error("Exception:: {}", rna.getStackTrace());
        final HttpStatus status = HttpStatus.UNAUTHORIZED;
        return new ResponseEntity<>(new ErrorResponse(
                HttpStatus.UNAUTHORIZED.value(),
                rna.getMessage(),
                null),
                status);
    }

    // ConstraintViolationException.class

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> validationException(final MethodArgumentNotValidException rna,
                                                              final HttpServletRequest request) {
        log.error("Exception:: {}", rna.getStackTrace());
        final HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(new ErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                rna.getBindingResult().getAllErrors().get(0).getDefaultMessage(),
                null),
                status);
    }
}
