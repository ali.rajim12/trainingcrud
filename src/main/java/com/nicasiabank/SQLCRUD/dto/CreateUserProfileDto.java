package com.nicasiabank.SQLCRUD.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * @author rajim 2021-09-23.
 * @project IntelliJ IDEA
 */
@Data
public class CreateUserProfileDto {

    @Size(max = 10, message = "Invalid length.")
    // @NotNull(message = "Username is required.")
    private String username;

    private String password;

    @Email(message = "Invalid Email Address")
    private String email;

    private String address;

    private String fullName;

    private String age;
}
