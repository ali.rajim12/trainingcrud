package com.nicasiabank.SQLCRUD.dto;

import lombok.Builder;
import lombok.Data;
import java.util.*;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
@Data
@Builder
public class UserActivityResource {
    private UserResource userResource;
    private List<ActivityDto> activityDtoList;
}

//{
//    userResource: {
//            name: ""
//        },
//        activityDtoList: [
//                      {
//                          status:
//                          remarks:
//        loginDateTime:
//        }
//                ]
//        }