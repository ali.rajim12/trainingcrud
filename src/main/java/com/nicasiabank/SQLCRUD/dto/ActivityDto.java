package com.nicasiabank.SQLCRUD.dto;

import com.nicasiabank.SQLCRUD.model.Status;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;

import java.util.Date;

/**
 * @author rajim 2021-09-26.
 * @project IntelliJ IDEA
 */
@Data
@Builder
public class ActivityDto {
    private Status status;
    private String remarks;
    private Date loginDateTime;
}
