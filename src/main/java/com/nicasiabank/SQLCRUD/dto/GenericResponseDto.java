package com.nicasiabank.SQLCRUD.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author rajim 2021-09-23.
 * @project IntelliJ IDEA
 */
@Data
@Builder
public class GenericResponseDto {
    private String message;
    private int code;
}
