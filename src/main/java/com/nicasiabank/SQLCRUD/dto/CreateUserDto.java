package com.nicasiabank.SQLCRUD.dto;

import com.nicasiabank.SQLCRUD.model.Status;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author rajim 2021-09-21.
 * @project IntelliJ IDEA
 */
@Data
public class CreateUserDto {

    @Size(max = 10, message = "Invalid length.")
   // @NotNull(message = "Username is required.")
    private String username;

    private String password;

    @Email(message = "Invalid Email Address")
    private String email;

    private String address;

}
