package com.nicasiabank.SQLCRUD.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author rajim 2021-09-22.
 * @project IntelliJ IDEA
 */
@Data
public class PageResult<T> {
    private List<T> results;

    private int page;

    private long totalResult;

    private int totalPages;

    public PageResult() {

    }

    public PageResult(List<T> results,
                      int page,
                      long totalResult,
                      int totalPages) {
        this.results = results;
        this.page = page;
        this.totalResult = totalResult;
        this.totalPages = totalPages;
    }
}
