package com.nicasiabank.SQLCRUD.dto;

import com.nicasiabank.SQLCRUD.model.Status;
import lombok.Builder;
import lombok.Data;

/**
 * @author rajim 2021-09-21.
 * @project IntelliJ IDEA
 */
@Data
@Builder
public class UserResource {
    private Long userId;
    private String username;
    private String email;
    private String address;
    private String status;
}
