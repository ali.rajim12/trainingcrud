package com.nicasiabank.SQLCRUD.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author rajim 2021-09-24.
 * @project IntelliJ IDEA
 */
@Data
@Builder
public class UserProfileResource {
    private UserResource userResource;
    private String fullName;
    private String age;
}
